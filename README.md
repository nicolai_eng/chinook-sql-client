# Chinook - SQL Client

## Description

A program which displays use of CRUD operations using SQL Client in a SqlServer file called Chinook.

## Installation

Chinook SqlServer: https://lms.noroff.no/pluginfile.php/232937/mod_assign/introattachment/0/Chinook_SqlServer_AutoIncrementPKs.sql?forcedownload=1

Install SSMS 18 and Microsoft SQL Server.

Install the project: 
Clone: https://gitlab.com/nicolai_eng/chinook-sql-client.git

## Setup

1. Open project in Visual Studio.
2. "Repository" folder -> "ConnectionStringHelper" file -> GetConnectionString() method.
3. Change string in "builder.DataSource" property to own personal SSMS server name.

```
```
## Contributors

* [NKE @nicolai_eng](@nicolai_eng)

