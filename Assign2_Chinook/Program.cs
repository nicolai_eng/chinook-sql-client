﻿using Assign2_Chinook.Models;
using Assign2_Chinook.Repositories;
using System.Reflection;
using System.Text;

namespace Assign2_Chinook
{
    /// <summary>
    /// Main program class.
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main method running when starting program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Create an object from each repository
            ICostumerRepository repository = new CostumerRepository();
            ICustomerCountryRepository countryRepository = new CustomerCountryRepository();
            ICustomerSpenderRepository spenderRepository = new CustomerSpenderRepository();
            ICustomerGenreRepository genreRepository = new CustomerGenreRepository();

            // Test for each of the methods creates inside the Repository: (uncomment dersied test to run) 
            TestSelectCustomer(repository);
            // TestSelectCustomers(repository);
            // TestInsert(repository);
            // TestUpdate(repository);
            // TestCountryCounter(countryRepository);
            // TestSpenderCounter(spenderRepository);
            // TestFavouriteGenre(genreRepository);
            
            // Stops program from exiting before a button is pressed!
            Console.ReadLine();
        }

        /// <summary>
        /// Tests the GetCustomer method from the CustomerRepository.
        /// </summary>
        static void TestSelectCustomer(ICostumerRepository repository)
        {
            // Prints data
            PrintItem<Customer>(repository.GetCustomer("CustomerID", "30")); // Write "CustomerID" to select by customerId and "FirstName" to select by name
        }

        /// <summary>
        /// Tests the GetCustomers method from the CustomerRepository.
        /// </summary>
        static void TestSelectCustomers(ICostumerRepository repository)
        {
            // Prints data
            PrintItems<Customer>(repository.GetCustomers(10, 10)); // Offset is 10 and chooses next 10 rows. No args for to print all rows 
        }

        /// <summary>
        /// Tests the AddNewCustomer method from the CustomerRepository.
        /// </summary>
        static void TestInsert(ICostumerRepository repository)
        {
            // Creates test Customer
            Customer test = new Customer()
            {
                FirstName = "Bob",
                LastName = "Bobby",
                Country = "USA",
                PostalCode = "35004",
                Phone = "0123456789",
                Email = "Bob.Bobby@gmail.com"
            };

            if (repository.AddNewCostumer(test))
            {
                // If succeeds print result + new customer line.
                Console.WriteLine($"{test.FirstName} was succesfully insertedd");
                PrintItem<Customer>(repository.GetCustomer("FirstName", test.FirstName));
            } else
            {
                // If error:
                Console.WriteLine("Uff... no insert");
            }
        }

        /// <summary>
        /// Tests the UpdateCustomer method from the CustomerRepository.
        /// </summary>
        static void TestUpdate(ICostumerRepository repository)
        {
            string Name = "Bob"; // Customer name which we wanna update.

            // Creates test object for update.
            Customer test = new Customer()
            {
                CustomerId = repository.GetCustomer("FirstName", Name).CustomerId,
                FirstName = "Bobb",
                LastName = "Bobby",
                Country = "USA",
                PostalCode = "35004",
                Phone = "0123456789",
                Email = "Bob.Bobby@gmail.com"
            };

            if (repository.UpdateCostumer(test))
            {
                // If succeeds print result + update customer line.
                Console.WriteLine($"{test.FirstName} was succesfully Updated");
                PrintItem<Customer>(repository.GetCustomer("CustomerId", $"{test.CustomerId}"));
            }
            else
            {
                // If error.
                Console.WriteLine("Uff... no insert");
            }
        }

        /// <summary>
        /// Tests the CustomerCountryCount method from the CustomerCountryRepository.
        /// </summary>
        static void TestCountryCounter(ICustomerCountryRepository countryRepository)
        {
            // Prints data
            PrintItems<CustomerCountry>(countryRepository.CustomerCountryCount()); 
        }

        /// <summary>
        /// Tests the CustomerSpenderCount method from the CustomerSpenderRepository.
        /// </summary>
        static void TestSpenderCounter(ICustomerSpenderRepository spenderRepository)
        {
            // Prints data
            PrintItems<CustomerSpender>(spenderRepository.CustomerSpenderCount());
        }

        /// <summary>
        /// Tests the CustomerFavouriteGenre method from the CustomerGenreRepository.
        /// </summary>
        static void TestFavouriteGenre(ICustomerGenreRepository genreRepository)
        {
            // Prints data
            PrintItems<CustomerGenre>(genreRepository.CustomerFavouriteGenre());
        }

        /// <summary>
        /// Prints data from input
        /// </summary>
        /// <typeparam name="T"> Type of input </typeparam>
        /// <param name="items"> Input data </param>
        static void PrintItems<T>(IEnumerable<T> items)
        {
            // Runs thorugh each row of input data
            foreach (T item in items)
            {
                // Plots each row.
                PrintItem<T>(item);
            }
        }

        /// <summary>
        /// Prints data row
        /// </summary>
        /// <typeparam name="T"> Type of input </typeparam>
        /// <param name="items"> Input row data </param>
        static void PrintItem<T>(T item)
        {
            /// Creates string containing each element/property of input object
            StringBuilder sb = new StringBuilder("---");
            foreach (PropertyInfo prop in item.GetType().GetProperties())
            {
                sb.Append($" {prop.GetValue(item)} ");
            }
            sb.Append("---");

            // Prints row string
            Console.WriteLine(sb.ToString());
        }
    }
}