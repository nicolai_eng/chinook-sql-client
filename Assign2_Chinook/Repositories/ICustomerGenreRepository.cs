﻿using Assign2_Chinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign2_Chinook.Repositories
{
    // CustomerGenre interface.
    public interface ICustomerGenreRepository
    {
        // Method that finds each customers favourite genre.
        public List<CustomerGenre> CustomerFavouriteGenre();
    }
}
