﻿using Assign2_Chinook.Models;

namespace Assign2_Chinook.Repositories
{
    // CustomerSpender interface
    public interface ICustomerSpenderRepository
    {
        // Functions that counts how much each customer has spend on tracks:
        public List<CustomerSpender> CustomerSpenderCount();
    }
}
