﻿using Assign2_Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// Class that holds all methods for Country related sql operations.
    /// </summary>
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Creates list of countries and the sum of customers lives there, in a descending order.
        /// </summary>
        /// <returns> List of countries and customer count comming from there </returns>
        public List<CustomerCountry> CustomerCountryCount()
        {
            // Creates list of CustomCountry objects.
            List<CustomerCountry> countries = new List<CustomerCountry>();

            // sql: Counts number of customers and groups them by country. Orders descendingly according to count.
            string sql = "SELECT Count(CustomerId), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC";

            try
            {
                // Connects to sql Server.
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (connection + sql string)
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Loops through all data rows.
                            while (reader.Read())
                            {
                                // Creates new CustomCountry object from data.
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(1);
                                temp.CountryCount = reader.GetInt32(0);

                                // Inserts new object in list.
                                countries.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Displays sql errors.
            }

            return countries; // Returns list of countries and their number of customer-inhabitants.
        }
    }
}
