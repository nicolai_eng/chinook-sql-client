﻿using Assign2_Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// Class that holds all methods for Genre related sql operations
    /// </summary>
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Finds the favourite genres of each customer.
        /// </summary>
        /// <returns> A list of customers and their favourite music genre </returns>
        public List<CustomerGenre> CustomerFavouriteGenre()
        {
            // Creates CustomGenre object list.
            List<CustomerGenre> genres = new List<CustomerGenre>();

            // sql: First creates nested query which selects CustomerId, GenreName and how many times the customer has heard
            // the given genre. This is done by connecting table InvoiceLine, Invoice, Genre and Track using INNER JOIN, 
            // and then grouping by CustomerId and GenreName.
            // Another query then uses the table from the nested query to filter out the combinations of CustomerId and genreName which
            // // has the largest count, for each CustomerId.
            string sql = "WITH nest AS " +
                "(SELECT Invoice.CustomerId, Genre.Name, COUNT(Track.GenreId) AS GenreCount " +
                "FROM TRACK " +
                "INNER JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId " +
                "INNER JOIN Invoice ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Genre ON Track.GenreId = Genre.GenreID " +
                "GROUP BY Genre.Name, Invoice.CustomerId) " +
                "SELECT CustomerId, Name FROM nest AS A1 " +
                "WHERE GenreCount = " +
                "(SELECT MAX(A2.GenreCount) FROM nest AS A2 " +
                "WHERE A1.CustomerId = A2.CustomerId) " +
                "ORDER BY CustomerId ASC ";

            try
            {
                // Connects to sql Server
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (sql string + connection)
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // loops through each data row obtained from sql.
                            while (reader.Read())
                            {
                                // Creates new CustomGenre object from data entry:
                                CustomerGenre genre = new CustomerGenre();
                                genre.CustomerID = reader.GetInt32(0);
                                genre.FavouriteGenre = reader.GetString(1);
                                // Adds object to list:
                                genres.Add(genre);
                            }
                        }
                    }
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Displays sql errors
            }

            return genres; // Returns list of customers and their favourite music genre
        }
    }
}
