﻿using Assign2_Chinook.Models;

namespace Assign2_Chinook.Repositories
{
    // Customer interface
    public interface ICostumerRepository
    {
        // Method that get a single customer by Id or Name:
        public Customer GetCustomer(string attribute, string value);

        // Method that gets all customers:
        public List<Customer> GetCustomers();

        // Method that gets subset of customers:
        public List<Customer> GetCustomers(int v1, int v2);

        // Method that adds new customer to table.
        public bool AddNewCostumer(Customer costumer);

        // Method that updates an existing customer.
        public bool UpdateCostumer(Customer costumer);
    }
}
