﻿using Assign2_Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// Class that holds all functionality/methods for CRUD (minus delete) operation in customer-table.
    /// Only the attribues given in the customer-class is inluded
    /// </summary>
    public class CostumerRepository : ICostumerRepository
    {
        /// <summary>
        /// Gets all entries in customer-table
        /// </summary>
        /// <returns> List of all customers in customer-table </returns>
        public List<Customer> GetCustomers()
        {
            List<Customer> custList = new List<Customer>(); // Creates + initializes new customer list.
            // sql: Selects desired attributes from customer table.
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer"; 
            try
            {
                // Connects to SQL server.
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString())) 
                {
                    conn.Open();

                    // Sets up sequal command (connection + sql string)
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read()) // Loops through each all revieved data rows
                            {
                                // Adds customer data row to customer list using ReadCustomer() method (found in Tools.cs)
                                custList.Add(reader.ReadCustomer());
                            }
                        }
                    }
                }
            } catch(SqlException ex) 
            {
                Console.WriteLine(ex.Message); // Displays error if SQL fails.
            }

            return custList; // Returns retrived customer list.
        }

        /// <summary>
        /// Overloaded GetCustomers() method.
        /// Gets "limVal" next customers starting from row "offval"
        /// </summary>
        /// <param name="offVal"> Starting row </param>
        /// <param name="limVal"> Amount of rows after "offVal"</param>
        /// <returns> List of "limVal" customers </returns>
        public List<Customer> GetCustomers(int offVal, int limVal)
        {
            List<Customer> custList = new List<Customer>();

            // Same sql as for GetCustomers() byt with and OFFSET and FETCH NEXT part added.
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId " +
                "OFFSET @offVal ROWS FETCH NEXT @limVal ROWS ONLY";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Inserts argument values inside sql string
                        cmd.Parameters.AddWithValue("@offVal", offVal);
                        cmd.Parameters.AddWithValue("@limVal", limVal);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                custList.Add(reader.ReadCustomer());
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }


            return custList; // Returns list of next amount = "limVal" customers coming after row "offVal".
        }

        /// <summary>
        /// Gets a single customer by either CustomerId or FirstName
        /// </summary>
        /// <param name="attribute"> Set to "CustomerId" to select by customerId or to "FirstName to select by name "</param>
        /// <param name="value"> Value which customer is selected from </param>
        /// <returns> Selected customer </returns>
        public Customer GetCustomer(string attribute, string value)
        {
            // Selects customer where condition (chosen by method argument) is true.
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE {attribute} = @value";
            try 
            {
                // Connect to SQL server
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (sql string + connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Inputs argument value inside sql string.
                        cmd.Parameters.AddWithValue("@value", value);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Returns single customer selected
                            reader.Read();
                            return reader.ReadCustomer(); // ReadCustomer() method is found in Tools.cs
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message); // Displays sql error.
            }

            return new Customer(); // Returns empty customer in case an error occured.
        }

        /// <summary>
        /// Adds a new customer to customer-table
        /// </summary>
        /// <param name="customer"> The new customer which we wish to add </param>
        /// <returns> A boolean which tells of if insertion was succesfull </returns>
        public bool AddNewCostumer(Customer customer)
        {
            bool success = false; // default value (unsuccesfull)
            // sql: Inserts values into desired attributes. CustomerId is not specified since primary key is to Identity (PK is set automatically).
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "Values(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                // Connects to SQL server
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (connection + sql string)
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Inserts new customer value into sql string.
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        // Returns success = true if any of the queries went through.
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }

            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Displays sql errors.
            }

            return success; // Returns the status of the insertion.
        }

        /// <summary>
        /// Updates a specific customer table entry.
        /// </summary>
        /// <param name="customer"> The updated customer entry </param>
        /// <returns> A boolean which tells of if insertion was succesfull </returns>
        public bool UpdateCostumer(Customer customer)
        {
            bool success = false; // default value (unsuccesfull)
            // sql: Finds desired entry from primary key (CustomerId) and sets the attributes equal to the customer from the method argument
            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email "
                + "WHERE CustomerId = @CustomerId";
            try
            {
                // Connection to SQL Server
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (sql string + connection)
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Inserts values from argument customer into the desired customer
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        // Returns success = true if any of the queries went through.
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Displays sql errors.
            }

            return success; // Returns the status of the insertion.
        }
    }
}
