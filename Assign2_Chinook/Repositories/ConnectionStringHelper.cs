﻿
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// Class for making connection to sql server easier.
    /// </summary>
    public class ConnectionStringHelper
    {
        /// <summary>
        /// Method that creates sql string for SQL server connection.
        /// </summary>
        /// <returns> SQL Connection string </returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "LAPTOP-19OAM899\\SQLEXPRESS";
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }
    }
}
