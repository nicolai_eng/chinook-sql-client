﻿using Assign2_Chinook.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// CustomerCountry interface.
    /// </summary>
    public interface ICustomerCountryRepository
    {
        // Method that calculates som of customers comming from each country.
        public List<CustomerCountry> CustomerCountryCount();
    }
}
