﻿using Assign2_Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    // Toolbox for other Repositories
    public static class Tools
    {
        /// <summary>
        /// Does the same as reader.GetString() but returns "NaN" if a value is null.
        /// </summary>
        /// <param name="reader"> The reader object from "Microsoft.Data.SqlClient" </param>
        /// <param name="index"> The index of the attribute we wanna return </param>
        /// <returns> Attribute element or "NaN" </returns>
        public static string SafeGetString(this SqlDataReader reader, int index)
        {
            if (!reader.IsDBNull(index)) 
                return reader.GetString(index);
            return "NaN";
        }

        /// <summary>
        /// Does the exact same as SafeGetString, but for integer instead of a string.
        /// <returns></returns>
        public static string SafeGetInt(this SqlDataReader reader, int index)
        {
            if (!reader.IsDBNull(index))
                return reader.GetInt32(index).ToString();
            return "NaN";
        }

        /// <summary>
        /// Reads all deserid customer values into a new customer object.
        /// </summary>
        /// <param name="reader"> The reader object from "Microsoft.Data.SqlClient" </param>
        /// <returns> The new customer object</returns>
        public static Customer ReadCustomer(this SqlDataReader reader)
        {
            // Creates new customer object.
            Customer cust = new Customer();
            try
            {
                // inserts all values into new object
                cust.CustomerId = reader.SafeGetInt(0);
                cust.FirstName = reader.SafeGetString(1);
                cust.LastName = reader.SafeGetString(2);
                cust.Country = reader.SafeGetString(3);
                cust.PostalCode = reader.SafeGetString(4); // GetValue().ToString To return empty string on Null
                cust.Phone = reader.SafeGetString(5);
                cust.Email = reader.SafeGetString(6);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Displays errors.
            }
            return cust; // returns new object
        }
    }
}
