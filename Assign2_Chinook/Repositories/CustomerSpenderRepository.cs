﻿using Assign2_Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Assign2_Chinook.Repositories
{
    /// <summary>
    /// Class that holds all methods for Spender related sql operations
    /// </summary>
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Lists how much each customer has spend on tracks, on ranks them in a descending order
        /// </summary>
        /// <returns> ranked list of customers and their spendings </returns>
        public List<CustomerSpender> CustomerSpenderCount()
        {
            // Creates list for CustomSpender class objects.
            List<CustomerSpender> spenders = new List<CustomerSpender>();

            // sql: Selects the sum of the Total attributes from Invoice table, and groups by customerId.
            // Then orders descendingly according to spendings of customers.
            string sql = "SELECT SUM(Total), CustomerId FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";

            try
            {
                // Connects to sql Server.
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    // Creates sql command (sql string + connection
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Loops through all data rows.
                            while (reader.Read())
                            {
                                // creates new CustomSpender entry.
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(1);
                                temp.TotalCount = (double) reader.GetDecimal(0);
                                // Appends new entry to list
                                spenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // displays sql errors
            }
            return spenders; // returns ranked list of customers and their spendings.
        }
    }
}
