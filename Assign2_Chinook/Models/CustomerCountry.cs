﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign2_Chinook.Models
{
    /// <summary>
    /// Country class; used to hold data about how many customers are from each different country.
    /// </summary>
    public class CustomerCountry
    {
        public string? Country { get; set; } // Country in question
        public int? CountryCount { get; set; } // How many customers are from the specific country.
    }
}
