﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign2_Chinook.Models
{
    /// <summary>
    /// Spender class; used to hold data about what customers spends most money.
    /// </summary>
    public class CustomerSpender
    {
        public int? CustomerId { get; set; }
        public double? TotalCount { get; set; } // Total sum of what customer has bought.
    }
}
