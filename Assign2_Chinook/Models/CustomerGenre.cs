﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assign2_Chinook.Models
{
    /// <summary>
    /// Genre class; used to hold data about customers favourite music genres.
    /// </summary>
    public class CustomerGenre
    {
        public int? CustomerID { get; set; }
        public string? FavouriteGenre { get; set; }

    }
}
